﻿using UnityEngine;

// This code came from https://wiki.unity3d.com/index.php/Singleton
namespace Utilities
{
    public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static bool isShuttingDown = false;
        private static object lockObject = new object();
        private static T instance;
    
        public static T Instance
        {
            get
            {
                if (isShuttingDown)
                {
                    return null;
                }
 
                lock (lockObject)
                {
                    if (instance == null)
                    {
                        instance = FindObjectOfType<T>();
                    
                        if (instance == null)
                        {
                            var singleton = new GameObject();
                            instance = singleton.AddComponent<T>();
                            singleton.name = instance.GetType().Name;
                        }
                    }
 
                    return instance;
                }
            }
        }

        protected virtual void Awake()
        {
            if (instance != null && instance != this)
            {
                Destroy(gameObject);
            }
        
            DontDestroyOnLoad(instance.gameObject);
        }

        private void OnApplicationQuit()
        {
            isShuttingDown = true;
        }
 
 
        private void OnDestroy()
        {
            isShuttingDown = true;
        }
    }
}
