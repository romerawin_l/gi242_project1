﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : BaseSpaceship, IDamagable
    {
        public event Action OnExploded;

        [SerializeField] private AudioClip playerFireSound;
        [SerializeField] private float playerFireSoundVolume = 0.5f;
        [SerializeField] private AudioClip playerExplodeSound;
        [SerializeField] private float playerExplodeSoundVolume = 0.5f;

        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet can't be null");
            Debug.Assert(gunPosition != null, "gunPosition can't be null");
            Debug.Assert(playerFireSound != null, "playerFireSound can't be null");
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            AudioSource.PlayClipAtPoint(playerFireSound,Camera.main.transform.position, playerFireSoundVolume);
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "Hp is more than zero");
            AudioSource.PlayClipAtPoint(playerExplodeSound,Camera.main.transform.position, playerExplodeSoundVolume);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}

