﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spaceship
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private int damage;
        [SerializeField] private float speed;
        [SerializeField] private new Rigidbody2D rigidbody2D;

        public void Init(Vector2 direction)
        {
            Move(direction);
        }

        private void Awake()
        {
            Debug.Assert(rigidbody2D != null, "rigidbody2D can't be null");
        }

        private void Move(Vector2 direction)
        {
            rigidbody2D.velocity = direction * speed;
            Destroy(gameObject,3f);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            var target = other.gameObject.GetComponent<IDamagable>();
            target?.TakeHit(damage);
        }
    }
}

