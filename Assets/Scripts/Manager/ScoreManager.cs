﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Utilities;

namespace Manager
{
    public class ScoreManager : MonoSingleton<ScoreManager>
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI finalScoreText;


        private GameManager gameManager;
        private int playerScore = 0;

        public void Init(GameManager gameManager)
        {
            this.gameManager = gameManager;
            this.gameManager.OnRestarted += OnRestarted;
            HideScore(false);
            SetScore(0);
            finalScoreText.text = "";
        }

        public void SetScore(int score)
        {
            scoreText.text = $"Score : {score}";
            playerScore = score;
        }

        protected override void Awake()
        {
            Debug.Assert(scoreText != null, "scoreText can't be null");
            Debug.Assert(finalScoreText != null, "finalScoreText can't be null");

        }

        private void OnRestarted()
        {
            finalScoreText.text = $"Player Score : {playerScore}";
            gameManager.OnRestarted -= OnRestarted;
            HideScore(true);
            SetScore(0);
        }

        private void HideScore(bool hide)
        {
            scoreText.gameObject.SetActive(!hide);
        }
    }
}

