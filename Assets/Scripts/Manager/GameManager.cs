﻿using UnityEngine;
using System;
using Spaceship;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Utilities;

namespace Manager
{
    public class GameManager : MonoSingleton<GameManager>
    { 
        [SerializeField] private Button startButton;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private ScoreManager scoreManager;
        [SerializeField] private RectTransform endScreen;
        [SerializeField] private Button replayButton;
        [SerializeField] private Button quitButton;
        [SerializeField] private Button nextButton;
        [SerializeField] private RectTransform messageDied;
        [SerializeField] private RectTransform messageWin;


        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;

        protected override void Awake()
        { 
            Debug.Assert(startButton != null, "startButton != null");
            Debug.Assert(dialog != null, "dialog != null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship != null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship != null");
            Debug.Assert(scoreManager != null, "scoreManager != null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceshipHp > 0");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed > 0");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp > 0");
            Debug.Assert(enemySpaceshipMoveSpeed > 0,"enemySpaceshipMoveSpeed > 0");
            Debug.Assert(endScreen != null, "endScreen != null");
            Debug.Assert(replayButton != null, "replayButton != null");
            Debug.Assert(quitButton != null, "quitButton != null");
            Debug.Assert(nextButton != null, "nextButton != null");
            Debug.Assert(messageDied != null, "messageDied != null");
            Debug.Assert(messageWin != null, "messageWin != null");

            startButton.onClick.AddListener(OnStartButtonClicked);
            replayButton.onClick.AddListener(OnReplayButtonClicked);
            quitButton.onClick.AddListener(OnQuitButtonClicked);
            nextButton.onClick.AddListener(OnNextButtonClicked);
        }
        
        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
        }

        private void OnReplayButtonClicked()
        {
            SceneManager.LoadScene("Game");
        }

        private void OnNextButtonClicked()
        {
            SceneManager.LoadScene("Stage2");
        }

        public void OnQuitButtonClicked()
        {
            Application.Quit();
        }
        
        private void StartGame()
        {
            scoreManager.Init(this);
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
        }
        
        private void SpawnPlayerSpaceship()
        {
            var spaceship = Instantiate(playerSpaceship);
            spaceship.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spaceship.OnExploded += OnPlayerSpaceshipExploded;
        }
        
        private void OnPlayerSpaceshipExploded()
        {
            Restart();
            nextButton.gameObject.SetActive(false);
            messageDied.gameObject.SetActive(true);
            messageWin.gameObject.SetActive(false);
        }
        
        private void SpawnEnemySpaceship()
        {
            var spaceship = Instantiate(enemySpaceship);
            spaceship.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spaceship.OnExploded += OnEnemySpaceshipExploded;
        }
        
        private void OnEnemySpaceshipExploded()
        {
            scoreManager.SetScore(1);
            Restart();
        }
        
        private void Restart()
        {
            DestroyRemainingShips();
            endScreen.gameObject.SetActive(true);
            OnRestarted?.Invoke();
        }

        private void DestroyRemainingShips()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemies)
            {
                Destroy(enemy);
            }
            
            remainingEnemies = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingEnemies)
            {
                Destroy(player);
            }
        }
    }
}

